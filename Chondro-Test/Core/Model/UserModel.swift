//
//  UserModel.swift
//  Chondro-Test
//
//  Created by MEKARI on 03/08/22.
//

import Foundation

struct UserModel: Decodable {
    let id: Int
    let name, username, email: String
    let address: Address
    let phone, website: String
    let company: Company
}

struct Address: Decodable {
    let street, suite, city, zipcode: String
    let geo: Geo
}

struct Geo: Decodable {
    let lat, lng: String
}

struct Company: Decodable {
    let name, catchPhrase, bs: String
}
