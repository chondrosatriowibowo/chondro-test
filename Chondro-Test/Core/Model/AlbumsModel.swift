//
//  AlbumsModel.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import Foundation

struct AlbumsModel:Decodable{
    let userId:Int
    let id:Int
    let title:String
}
