//
//  CommentModel.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import Foundation

struct CommentModel:Decodable{
    let postId:Int
    let id:Int
    let name:String
    let email:String
    let body:String
}
