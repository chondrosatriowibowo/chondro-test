//
//  PostModel.swift
//  Chondro-Test
//
//  Created by MEKARI on 03/08/22.
//

import Foundation

struct PostModel:Decodable{
    let userId:Int
    let id:Int
    let title:String
    let body:String
}
