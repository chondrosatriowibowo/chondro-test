//
//  PhotoModel.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import Foundation

struct PhotoModel:Decodable{
    let albumId:Int
    let id:Int
    let title:String
    let url:String
    let thumbnailUrl:String
}
