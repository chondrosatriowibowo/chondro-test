//
//  ApiClient.swift
//  Chondro-Test
//
//  Created by MEKARI on 03/08/22.
//

import Foundation
import Alamofire

class ApiClient{
    static let shared = ApiClient()
    func request<T:Decodable>(url:String,method:HTTPMethod,callback:@escaping(Result<T,AFError>)->Void){
        AF.request(url,method:method).responseDecodable(of:T.self){ response in
            callback(response.result)
        }
    }
}
