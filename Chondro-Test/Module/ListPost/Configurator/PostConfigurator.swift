//
//  PostConfigurator.swift
//  Chondro-Test
//
//  Created by MEKARI on 03/08/22.
//

import Foundation

enum PostConfigurator{
    static func configure(viewController:PostTableViewCell){
        let presenter = PostPresenter(viewController: viewController)
        let worker = ListPostWorker()
        let interactor = PostInteractor(worker: worker, presenter: presenter)
        viewController.interactor = interactor
    }
}
