//
//  ListPostConfigurator.swift
//  Chondro-Test
//
//  Created by MEKARI on 03/08/22.
//

import Foundation

enum ListPostConfigurator{
    static func configure(viewController:ListPostViewController){
        let presenter = ListPostPresenter(viewController: viewController)
        let worker = ListPostWorker()
        let interactor = ListPostInteractor(worker: worker, presenter: presenter)
        viewController.interactor = interactor
    }
}
