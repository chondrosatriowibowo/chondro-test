//
//  PostTableViewCell.swift
//  Chondro-Test
//
//  Created by MEKARI on 03/08/22.
//

import UIKit

class PostTableViewCell: UITableViewCell, PostPresenterOutput {
    
    

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    var getUserId:((Int)->Void)?
    var interactor:PostInteractor?
    var post:PostModel?{
        didSet{
            if let post = post {
                self.interactor?.getData(userId: post.userId)
                setData(post: post)
            }
        }
    }
    
    var user:UserModel?{
        didSet{
            if let user = user {
                setData(user: user)
            }
        }
    }
    
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        PostConfigurator.configure(viewController: self)
        // Initialization code
    }
    
    private func setData(post:PostModel){
        titleLabel.text = post.title
        bodyLabel.text = post.body
        getUserId?(post.userId)
    }
    
    private func setData(user:UserModel){
        nameLabel.text = user.name
        companyLabel.text = user.company.name
    }
    
    class func nib()-> UINib{
        UINib(nibName: "PostTableViewCell", bundle: nil)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func get(data: UserModel) {
        self.setData(user: data)
    }
    
    func error(message: String) {
        
    }
    
}
