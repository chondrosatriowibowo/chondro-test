//
//  ListPostViewController.swift
//  Chondro-Test
//
//  Created by MEKARI on 03/08/22.
//

import UIKit

class ListPostViewController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var interactor:ListPostInteractor?
    
    var countList:Int = 20
    var index:Int = 0
    var isLoading:Bool = true
    
    var listPost:[PostModel] = []
    var users:[UserModel] = []
    init() {
        super.init(nibName: "ListPostViewController", bundle: nil)
        ListPostConfigurator.configure(viewController: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appear()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableview()
        // Do any additional setup after loading the view.
    }

    private func setupTableview(){
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(PostTableViewCell.nib(), forCellReuseIdentifier: "PostTableViewCell")
    }
    
    private func appear(){
        self.interactor?.getListPost()
        self.tableview.isHidden = true
        self.activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ListPostViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        isLoading == true ? 0 : countList
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isLoading == true {
            return UITableViewCell()
        } else {
            let cell:PostTableViewCell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell") as! PostTableViewCell
            cell.post = listPost[indexPath.row]
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = DetailPostViewController()
        controller.post = listPost[indexPath.row]
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

extension ListPostViewController: ListPostPresenterOutput{
    func get(data: UserModel) {
    }
    
    func getList(posts: [PostModel]) {
        while index < countList {
            listPost.append(posts[index])
            index += 1
        }
        getAllData()
    }
    
    func error(message: String) {
        
    }
    
    private func getAllData(){
        tableview.isHidden = false
        activityIndicator.isHidden = true
        activityIndicator.stopAnimating()
        isLoading = false
        tableview.reloadData()
    }
}
