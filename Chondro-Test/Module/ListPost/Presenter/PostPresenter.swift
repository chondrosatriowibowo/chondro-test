//
//  PostPresenter.swift
//  Chondro-Test
//
//  Created by MEKARI on 03/08/22.
//

import Foundation

typealias PostPresenterInput = PostInteractorOutput

protocol PostPresenterOutput{
    func get(data:UserModel)
    func error(message:String)
}

class PostPresenter: PostPresenterInput{
    
    var viewController:PostPresenterOutput?
    
    init(viewController:PostPresenterOutput) {
        self.viewController = viewController
    }
    
    func error(message: String) {
        self.viewController?.error(message: message)
    }
    
    func get(data: UserModel) {
        self.viewController?.get(data: data)
    }
}
