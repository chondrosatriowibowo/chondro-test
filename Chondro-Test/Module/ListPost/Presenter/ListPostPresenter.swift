//
//  ListPostPresenter.swift
//  Chondro-Test
//
//  Created by MEKARI on 03/08/22.
//

import Foundation

typealias ListPostPresenterInput = ListPostInteractorOutput

protocol ListPostPresenterOutput{
    func getList(posts:[PostModel])
    func get(data:UserModel)
    func error(message:String)
}

class ListPostPresenter: ListPostPresenterInput{
    
    var viewController:ListPostPresenterOutput?
    
    init(viewController:ListPostPresenterOutput) {
        self.viewController = viewController
    }
    
    func getList(posts: [PostModel]) {
        self.viewController?.getList(posts: posts)
    }
    
    func error(message: String) {
        self.viewController?.error(message: message)
    }
    
    func get(data: UserModel) {
        self.viewController?.get(data: data)
    }
}
