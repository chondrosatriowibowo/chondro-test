//
//  PostInteractor.swift
//  Chondro-Test
//
//  Created by MEKARI on 03/08/22.
//

import Foundation

protocol PostInteractorInput{
    func getData(userId:Int)
}

protocol PostInteractorOutput{
    func get(data:UserModel)
    func error(message:String)
}

class PostInteractor{
    var worker:ListPostWorker?
    var presenter:PostInteractorOutput?
    init(worker:ListPostWorker,presenter:PostInteractorOutput) {
        self.worker = worker
        self.presenter = presenter
    }
}

extension PostInteractor:PostInteractorInput{
    func getData(userId: Int) {
        worker?.getUser(userId: userId){(response) in
            DispatchQueue.main.async {
                switch response{
                case .success(let data):
                    self.presenter?.get(data: data)
                case .failure(let error):
                    self.presenter?.error(message: error.localizedDescription)
                }
            }
        }
    }
    
    
}
