//
//  ListPostInteractor.swift
//  Chondro-Test
//
//  Created by MEKARI on 03/08/22.
//

import Foundation

protocol ListPostInteractorInput{
    func getListPost()
    func getData(userId:Int)
}

protocol ListPostInteractorOutput{
    func getList(posts:[PostModel])
    func get(data:UserModel)
    func error(message:String)
}

class ListPostInteractor{
    var worker:ListPostWorker?
    var presenter:ListPostInteractorOutput?
    init(worker:ListPostWorker,presenter:ListPostInteractorOutput) {
        self.worker = worker
        self.presenter = presenter
    }
}

extension ListPostInteractor:ListPostInteractorInput{
    func getData(userId: Int) {
        worker?.getUser(userId: userId){(response) in
            DispatchQueue.main.async {
                switch response{
                case .success(let data):
                    self.presenter?.get(data: data)
                case .failure(let error):
                    self.presenter?.error(message: error.localizedDescription)
                }
            }
        }
    }
    
    func getListPost() {
        worker?.getList{ response in
            DispatchQueue.main.async {
                switch response{
                case .success(let data):
                    self.presenter?.getList(posts: data)
                case .failure(let error):
                    self.presenter?.error(message: error.localizedDescription)
                }
            }
        }
    }
}
