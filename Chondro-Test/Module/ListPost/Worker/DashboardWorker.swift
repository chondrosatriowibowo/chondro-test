//
//  DashboardWorker.swift
//  Chondro-Test
//
//  Created by MEKARI on 03/08/22.
//

import Foundation
import Alamofire
import UIKit


protocol ListPostSceneWorker{
    func getList(callback:@escaping(Result<[PostModel],AFError>)->Void)
    func getUser(userId:Int,callback:@escaping(Result<UserModel,AFError>)->Void)
}

class ListPostWorker{
    let service = ApiClient.shared
}

extension ListPostWorker:ListPostSceneWorker{
    func getUser(userId:Int,callback: @escaping (Result<UserModel, AFError>) -> Void) {
        service.request(url: "https://jsonplaceholder.typicode.com/users/\(userId)", method: .get){(result:Result<UserModel, AFError>) in
            switch result{
            case .success(let data):
                callback(.success(data))
            case .failure(let error):
                callback(.failure(error))
            }
        }
    }
    
    func getList(callback: @escaping (Result<[PostModel], AFError>) -> Void) {
        service.request(url: "https://jsonplaceholder.typicode.com/posts", method: .get){ (result:Result<[PostModel], AFError>) in
            switch result{
            case .success(let data):
                callback(.success(data))
            case .failure(let error):
                callback(.failure(error))
            }
        }
    }
}
