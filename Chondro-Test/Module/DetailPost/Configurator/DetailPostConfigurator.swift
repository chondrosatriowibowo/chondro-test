//
//  DetailPostConfigurator.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import Foundation

enum DetailPostConfigurator{
    static func configure(viewController:DetailPostViewController){
        let presenter = DetailPostPresenter(viewController: viewController)
        let worker = DetailPostWorker()
        let interactor = DetailPostInteractor(worker: worker, presenter: presenter)
        viewController.interactor = interactor
    }
}
