//
//  DetailPostWorker.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import Foundation
import Alamofire

protocol DetailPostScene{
    func getDetailPost(by id:Int,callback:@escaping(Result<PostModel,AFError>)->Void)
    func getUser(by id:Int,callback:@escaping(Result<UserModel,AFError>)->Void)
    func getComments(byPost id:Int,callback:@escaping(Result<[CommentModel],AFError>)->Void)
}

class DetailPostWorker{
    let service = ApiClient.shared
}

extension DetailPostWorker:DetailPostScene{
    func getDetailPost(by id: Int, callback: @escaping (Result<PostModel, AFError>) -> Void) {
        service.request(url: "https://jsonplaceholder.typicode.com/posts/\(id)", method: .get){(result:Result<PostModel, AFError>) in
            callback(result)
        }
    }
    
    func getUser(by id: Int, callback: @escaping (Result<UserModel, AFError>) -> Void) {
        service.request(url: "https://jsonplaceholder.typicode.com/users/\(id)", method: .get){(result:Result<UserModel, AFError>) in
            callback(result)
        }
    }
    
    func getComments(byPost id: Int, callback: @escaping (Result<[CommentModel], AFError>) -> Void) {
        service.request(url: "https://jsonplaceholder.typicode.com/posts/\(id)/comments", method: .get){(result:Result<[CommentModel],AFError>) in
            callback(result)
        }
    }
}
