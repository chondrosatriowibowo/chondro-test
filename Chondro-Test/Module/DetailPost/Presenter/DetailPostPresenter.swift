//
//  DetailPostPresenter.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import Foundation

typealias DetailPostPresenterInput = DetailPostInteractorOutput

protocol DetailPostPresenterOutput{
    func getDetail(data:PostModel)
    func get(data:UserModel)
    func getList(data:[CommentModel])
    func error(message:String)
}

class DetailPostPresenter:DetailPostPresenterInput{
    
    var viewController: DetailPostPresenterOutput?
    
    init(viewController:DetailPostPresenterOutput) {
        self.viewController = viewController
    }
    
    func getDetail(data: PostModel) {
        self.viewController?.getDetail(data: data)
    }
    
    func get(data: UserModel) {
        self.viewController?.get(data: data)
    }
    
    func getList(data: [CommentModel]) {
        self.viewController?.getList(data: data)
    }
    
    func error(message: String) {
        self.viewController?.error(message: message)
    }
}
