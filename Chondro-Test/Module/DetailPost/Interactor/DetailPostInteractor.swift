//
//  DetailPostInteractor.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import Foundation

protocol DetailPostInteractorInput{
    func getDetailPost(by id: Int)
    func getComments(by idPost:Int)
    func getUser(id: Int)
}

protocol DetailPostInteractorOutput{
    func getDetail(data:PostModel)
    func get(data:UserModel)
    func getList(data:[CommentModel])
    func error(message:String)
}

class DetailPostInteractor{
    var worker: DetailPostWorker?
    var presenter: DetailPostInteractorOutput?
    init(worker: DetailPostWorker,presenter: DetailPostInteractorOutput) {
        self.worker = worker
        self.presenter = presenter
    }
}

extension DetailPostInteractor: DetailPostInteractorInput{
    func getDetailPost(by id: Int) {
        worker?.getDetailPost(by: id){ callback in
            DispatchQueue.main.async {
                switch callback{
                case .success(let data):
                    self.presenter?.getDetail(data: data)
                case .failure(let error):
                    self.presenter?.error(message: error.localizedDescription)
                }
            }
        }
    }
    
    func getComments(by idPost: Int) {
        worker?.getComments(byPost: idPost){ callback in
            DispatchQueue.main.async {
                switch callback{
                case .success(let data):
                    self.presenter?.getList(data: data)
                case .failure(let error):
                    self.presenter?.error(message: error.localizedDescription)
                }
            }
        }
    }
    
    func getUser(id: Int) {
        worker?.getUser(by: id){ callback in
            DispatchQueue.main.async {
                switch callback{
                case .success(let data):
                    self.presenter?.get(data: data)
                case .failure(let error):
                    self.presenter?.error(message: error.localizedDescription)
                }
            }
        }
    }
}
