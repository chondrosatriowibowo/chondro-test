//
//  DetailPostViewController.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import UIKit

enum DetailStructure{
    case postDetail
    case comments
}

class DetailPostViewController: UIViewController {

    var interactor:DetailPostInteractor?
    
    @IBOutlet weak var tableview: UITableView!
    
    var post:PostModel?
    var user:UserModel?
    var comments:[CommentModel] = []
    var isLoading:Bool = true
    let structure:[DetailStructure] = [.postDetail,.comments]
    
    init() {
        super.init(nibName: "DetailPostViewController", bundle: nil)
        DetailPostConfigurator.configure(viewController: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.interactor?.getComments(by: post?.id ?? 0)
        self.interactor?.getUser(id: post!.userId)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableview()
        setupNavigation()
        // Do any additional setup after loading the view.
    }
    
    private func setupNavigation(){
        self.navigationItem.title = "Detail Post"
    }
    
    private func setupDataDone(){
        isLoading = false
        tableview.reloadData()
    }

    private func setupTableview(){
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(CommentTableViewCell.nib(), forCellReuseIdentifier: "CommentTableViewCell")
        tableview.register(DetailPostTableViewCell.nib(), forCellReuseIdentifier: "DetailPostTableViewCell")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailPostViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch structure[section]{
        case .postDetail :
            return nil
        case .comments :
            return "Comments"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch structure[section]{
        case .postDetail :
            return 0.0001
        case .comments :
            return 32
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoading == true {
            return 0
        } else {
            return numberCountTable(section: section)
        }
    }
    
    private func numberCountTable(section:Int)->Int{
        switch structure[section]{
        case .postDetail:
            return 1
        case .comments:
            return comments.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isLoading == true {
            return UITableViewCell()
        }
        else {
            return tableviewCell(tableview: tableview, indexPath: indexPath)
        }
    }
    
    private func tableviewCell(tableview: UITableView,indexPath:IndexPath)->UITableViewCell{
        switch structure[indexPath.section]{
        case .postDetail:
            let cell:DetailPostTableViewCell = tableview.dequeueReusableCell(withIdentifier: "DetailPostTableViewCell", for: indexPath) as! DetailPostTableViewCell
            cell.post = post
            cell.user = user
            cell.actionHandler = {
                let vc = DetailUserViewController()
                vc.user = self.user
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return cell
        case .comments:
            let cell:CommentTableViewCell = tableview.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
            cell.comment = comments[indexPath.row]
            return cell
        }
    }
    
    
}
extension DetailPostViewController: DetailPostPresenterOutput{
    func getDetail(data: PostModel) {
        
    }
    
    func get(data: UserModel) {
        user = data
    }
    
    func getList(data: [CommentModel]) {
        comments += data
        setupDataDone()
    }
    
    func error(message: String) {
        
    }
    
    
}
