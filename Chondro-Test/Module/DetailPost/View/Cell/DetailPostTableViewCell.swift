//
//  DetailPostTableViewCell.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import UIKit

class DetailPostTableViewCell: UITableViewCell {

    @IBOutlet weak var nameUserLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var titlePostLabel: UILabel!
    @IBOutlet weak var bodyPostLabel: UILabel!
    
    var post:PostModel?{
        didSet{
            if let post = post {
                setData(post: post)
            }
        }
    }
    
    var actionHandler:(()->Void)?
    
    var user:UserModel?{
        didSet{
            if let user = user {
                setData(user: user)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    private func setData(post:PostModel){
        titlePostLabel.text = post.title
        bodyPostLabel.text = post.body
    }
    
    private func setData(user:UserModel){
        nameUserLabel.text = user.name
        nameUserLabel.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(navigationUserDetail))
        nameUserLabel.addGestureRecognizer(gesture)
        companyNameLabel.text = user.company.name
    }
    
    @objc
    func navigationUserDetail(){
        actionHandler?()
    }
    
    class func nib()-> UINib{
        UINib(nibName: "DetailPostTableViewCell", bundle: nil)
    }
}
