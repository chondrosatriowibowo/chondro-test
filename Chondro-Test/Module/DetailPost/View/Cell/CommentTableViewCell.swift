//
//  CommentTableViewCell.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var nameUserLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    var comment:CommentModel?{
        didSet{
            if let comment = comment {
                set(data: comment)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    private func set(data:CommentModel){
        nameUserLabel.text = data.name
        bodyLabel.text = data.body
    }
    
    class func nib()->UINib{
        UINib(nibName: "CommentTableViewCell", bundle: nil)
    }
}
