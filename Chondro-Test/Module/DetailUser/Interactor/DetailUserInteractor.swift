//
//  DetailUserInteractor.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import Foundation

protocol DetailUserInteractorInput{
    func getData(by userId:Int)
    func getListPost(by userId:Int)
    func getAlbums(by userId:Int)
    func getPhotos(by albumId:Int)
}

protocol DetailUserInteractorOutput{
    func getUser(data:UserModel)
    func getListPost(data:[PostModel])
    func getListAlbums(data:[AlbumsModel])
    func getListPhoto(data:[PhotoModel])
    func error(message:String)
}

class DetailUserInteractor{
    var worker:DetailUserWorker?
    var presenter:DetailUserInteractorOutput?
    init(worker:DetailUserWorker,presenter:DetailUserInteractorOutput) {
        self.worker = worker
        self.presenter = presenter
    }
}

extension DetailUserInteractor:DetailUserInteractorInput{
    func getPhotos(by albumId: Int) {
        self.worker?.getPhotos(by: albumId){ response in
            DispatchQueue.main.async {
                switch response{
                case .success(let data):
                    self.presenter?.getListPhoto(data: data)
                case .failure(let error):
                    self.presenter?.error(message: error.localizedDescription)
                }
            }
        }
    }
    
    func getData(by userId: Int) {
        self.worker?.getUser(by: userId){ response in
            DispatchQueue.main.async {
                switch response{
                case .success(let data):
                    self.presenter?.getUser(data: data)
                case .failure(let error):
                    self.presenter?.error(message: error.localizedDescription)
                }
            }
        }
    }
    
    func getListPost(by userId: Int) {
        self.worker?.getPosts(by: userId){ response in
            DispatchQueue.main.async {
                switch response{
                case .success(let data):
                    self.presenter?.getListPost(data: data)
                case .failure(let error):
                    self.presenter?.error(message: error.localizedDescription)
                }
            }
        }
    }
    
    func getAlbums(by userId: Int) {
        self.worker?.getAlbums(by: userId){
            response in
                DispatchQueue.main.async {
                    switch response{
                    case .success(let data):
                        self.presenter?.getListAlbums(data: data)
                    case .failure(let error):
                        self.presenter?.error(message: error.localizedDescription)
                    }
                }
        }
    }
    
    
}
