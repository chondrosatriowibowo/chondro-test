//
//  AlbumsTableViewCell.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import UIKit

class AlbumsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var interactor:DetailUserInteractor?
    var photos:[PhotoModel] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCollection()
        DetailUserConfigurator.configure(cell: self)
    }

    var albums:AlbumsModel?{
        didSet{
            if let albums = albums{
                self.interactor?.getPhotos(by: albums.id)
                setAlbums(data: albums)
            }
        }
    }
    
    private func setupCollection(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(PhotoCollectionViewCell.nib(), forCellWithReuseIdentifier: "PhotoCollectionViewCell")
    }
    
    private func setAlbums(data:AlbumsModel){
        titleLabel.text = data.title
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    class func nib()-> UINib{
        UINib(nibName: "AlbumsTableViewCell", bundle: nil)
    }
    
}

extension AlbumsTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 72, height: 96)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:PhotoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as! PhotoCollectionViewCell
        cell.photo = photos[indexPath.row]
        return cell
    }
    
    
}

extension AlbumsTableViewCell: DetailUserPresenterOutput{
    func getListPhoto(data: [PhotoModel]) {
        print(data)
        photos += data
        collectionView.reloadData()
    }
    
    func getUser(data: UserModel) {
        
    }
    
    func getListPost(data: [PostModel]) {
        
    }
    
    func getListAlbums(data: [AlbumsModel]) {
        
    }
    
    func error(message: String) {
        
    }
    
    
}
