//
//  UserInfoTableViewCell.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import UIKit

class UserInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    
    var user:UserModel?{
        didSet{
            if let user = user {
                setup(data: user)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    private func setup(data:UserModel){
        self.nameLabel.text = data.name
        self.emailLabel.text = data.email
        self.addressLabel.text = "\(data.address.street),\(data.address.city) - \(data.address.zipcode)"
        self.companyLabel.text = data.company.name
    }
    
    class func nib()->UINib{
        UINib(nibName: "UserInfoTableViewCell", bundle: nil)
    }

}
