//
//  PhotoCollectionViewCell.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import UIKit
import SDWebImage

class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var photo:PhotoModel?{
        didSet{
            if let photo = photo {
                setData(photo: photo)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    private func setData(photo:PhotoModel){
        if let url = URL(string: photo.thumbnailUrl){
            imageView.sd_setImage(with: url)
            titleLabel.text = photo.title
        }
    }
    
    class func nib()-> UINib{
        UINib(nibName: "PhotoCollectionViewCell", bundle: nil)
    }

}
