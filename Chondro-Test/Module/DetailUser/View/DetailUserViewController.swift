//
//  DetailUserViewController.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import UIKit

enum DetailUserComponent{
    case userInfo
    case userAlbums
}
class DetailUserViewController: UIViewController {
    
    var interactor:DetailUserInteractor?
    @IBOutlet weak var tableview: UITableView!
    
    var user:UserModel?
    var posts:[PostModel] = []
    var album:[AlbumsModel] = []
    var structure:[DetailUserComponent] = [.userInfo,.userAlbums]
    var isLoading:Bool = true
    init() {
        super.init(nibName: "DetailUserViewController", bundle: nil)
        DetailUserConfigurator.configure(viewController: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.interactor?.getAlbums(by: user?.id ?? 0)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableview()
        // Do any additional setup after loading the view.
    }

    private func setupTableview(){
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(UserInfoTableViewCell.nib(), forCellReuseIdentifier: "UserInfoTableViewCell")
        tableview.register(AlbumsTableViewCell.nib(), forCellReuseIdentifier: "AlbumsTableViewCell")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailUserViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return structure.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoading == true {
            return 0
        } else {
            return numberRow(section: section)
        }
    }
    
    private func numberRow(section:Int)->Int {
        switch structure[section]{
        case .userInfo : return 1
        case .userAlbums : return album.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch structure[indexPath.section]{
        case .userInfo:
            return UIScreen.main.bounds.size.height * 0.3
        case .userAlbums:
            return 144
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isLoading == true{
            return UITableViewCell()
        } else {
            return tableviewCell(tableView: tableView, indexPath: indexPath)
        }
    }
    
    private func tableviewCell(tableView:UITableView,indexPath:IndexPath)->UITableViewCell{
        switch structure[indexPath.section]{
        case .userInfo :
            let cell:UserInfoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UserInfoTableViewCell", for: indexPath) as! UserInfoTableViewCell
            cell.user = user
            return cell
        case .userAlbums :
            let cell:AlbumsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AlbumsTableViewCell", for: indexPath) as! AlbumsTableViewCell
            cell.albums = album[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    
}

extension DetailUserViewController: DetailUserPresenterOutput{
    func getListPhoto(data: [PhotoModel]) {
        
    }
    
    func getUser(data: UserModel) {
        
    }
    
    func getListPost(data: [PostModel]) {
        
    }
    
    func getListAlbums(data: [AlbumsModel]) {
        album += data
        isLoading = false
        tableview.reloadData()
    }
    
    func error(message: String) {
        
    }
    
    
}
