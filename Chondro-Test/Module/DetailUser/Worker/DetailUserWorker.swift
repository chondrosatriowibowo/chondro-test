//
//  DetailUserWorker.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import Foundation
import Alamofire

protocol DetailUserSceneWorker{
    func getUser(by id:Int,callback:@escaping(Result<UserModel,AFError>)->Void)
    func getAlbums(by id:Int,callback:@escaping(Result<[AlbumsModel],AFError>)->Void)
    func getPosts(by userId:Int,callback:@escaping(Result<[PostModel],AFError>)->Void)
    func getPhotos(by id:Int,callback:@escaping(Result<[PhotoModel],AFError>)->Void)
}

class DetailUserWorker{
    let service = ApiClient.shared
}

extension DetailUserWorker:DetailUserSceneWorker{
    func getPhotos(by id: Int, callback: @escaping (Result<[PhotoModel], AFError>) -> Void) {
        service.request(url: "https://jsonplaceholder.typicode.com/albums/\(id)/photos", method: .get){ (response:Result<[PhotoModel], AFError>) in
            print(response)
            callback(response)
        }
    }
    
    func getUser(by id: Int, callback: @escaping (Result<UserModel, AFError>) -> Void) {
        service.request(url: "https://jsonplaceholder.typicode.com/users/\(id)", method: .get){(result:Result<UserModel, AFError>) in
            callback(result)
        }
    }
    
    func getAlbums(by id: Int, callback: @escaping (Result<[AlbumsModel], AFError>) -> Void) {
        service.request(url: "https://jsonplaceholder.typicode.com/users/\(id)/albums", method: .get){
            (result:Result<[AlbumsModel], AFError>) in
                callback(result)
        }
    }
    
    func getPosts(by userId: Int, callback: @escaping (Result<[PostModel], AFError>) -> Void) {
        service.request(url: "https://jsonplaceholder.typicode.com/users/\(userId)/posts", method: .get){
            (result:Result<[PostModel], AFError>) in
                callback(result)
        }
    }
    
    
}
