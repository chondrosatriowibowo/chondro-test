//
//  DetailUserConfigurator.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import Foundation

enum DetailUserConfigurator{
    static func configure(viewController:DetailUserViewController){
        let presenter = DetailUserPresenter(viewController: viewController)
        let worker = DetailUserWorker()
        let interactor = DetailUserInteractor(worker: worker, presenter: presenter)
        viewController.interactor = interactor
    }
    static func configure(cell:AlbumsTableViewCell){
        let presenter = DetailUserPresenter(viewController: cell)
        let worker = DetailUserWorker()
        let interactor = DetailUserInteractor(worker: worker, presenter: presenter)
        cell.interactor = interactor
    }
}
