//
//  DetailUserPresenter.swift
//  Chondro-Test
//
//  Created by MEKARI on 04/08/22.
//

import Foundation

typealias DetailUserPresenterInput = DetailUserInteractorOutput

protocol DetailUserPresenterOutput{
    func getUser(data:UserModel)
    func getListPost(data:[PostModel])
    func getListAlbums(data:[AlbumsModel])
    func error(message:String)
    func getListPhoto(data:[PhotoModel])
}

class DetailUserPresenter:DetailUserPresenterInput{

    var viewController:DetailUserPresenterOutput?
    
    init(viewController:DetailUserPresenterOutput) {
        self.viewController = viewController
    }
    
    func getUser(data: UserModel) {
        self.viewController?.getUser(data: data)
    }
    
    func getListPost(data: [PostModel]) {
        self.viewController?.getListPost(data: data)
    }
    
    func getListAlbums(data: [AlbumsModel]) {
        self.viewController?.getListAlbums(data: data)
    }
    
    func error(message: String) {
        self.viewController?.error(message: message)
    }
    
    func getListPhoto(data: [PhotoModel]) {
        self.viewController?.getListPhoto(data: data)
    }
}
